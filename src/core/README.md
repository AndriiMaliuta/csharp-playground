# csharp-playground

Type (Literal Suffix)
Description
Values/Range
byte 8-bit unsigned integer 0 to 255
sbyte 8-bit signed integer -128 to 127
short 16-bit signed integer -32,768 to 32,767
ushort 16-bit unsigned integer 0 to 65,535
int 32-bit signed integer -2,147,483,648 to 2,147,483,647
uint 32-bit unsigned integer 0 to 4,294,967,295
long (l) 64-bit signed integer –9,223,372,036,854,775,808 to
9,223,372,036,854,775,807
ulong (ul) 64-bit unsigned integer 0 to 18,446,744,073,709,551,615
float (f) 32-bit floating point -3.4 × 10 38 to +3.4 × 10 38
double (d) 64-bit floating point ±5.0 × 10 −324 to ±1.7 × 10 308
decimal (m) 128-bit, 28 or 29 digits of
precision (ideal for financial) (-7.9 × 10 28 to 7.9 x 10 28 ) / (10 0 to 28 )
bool Boolean true or false
char 16-bit Unicode character
(use single quotes) U+0000 to U+FFFF
string Sequence of Unicode
characters (use double
quotes) E.g., “abc”

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

> > > > > > > 1059fe5d4d14f64756c06d72954f40bef4f56776
