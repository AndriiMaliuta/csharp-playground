
string name = "Joe";
string helloViaConcatenation = "Hello, " + name + "!";
Console.WriteLine(helloViaConcatenation);

string helloViaStringFormat = string.Format("Hello, {0}!", name);
Console.WriteLine(helloViaStringFormat);

//C# 6 introduced a new way to format strings, called string interpolation.
Console.WriteLine($"{item}     {amount}");