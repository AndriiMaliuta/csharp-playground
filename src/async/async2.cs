using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

public class UserInfo
{
    public string Info { get; set; }
    public string Address { get; set; }
}
class UserService
{
    internal static async Task<string> GetUserAsync(string user)
    91{
    // Do some long running synchronous processing.
    return await Task.FromResult(user);
}
}
class AddressService
{
    internal static async Task<string> GetAddressAsync(string user)
{
return await Task.FromResult(user);
}
}
public class UserSearch
{
    public async Task<UserInfo> GetUserInfoAsync(string term, List<string> names)
    {
        var userName =
        (from name in names
        where name.StartsWith(term)
        select name)
        .FirstOrDefault();
        var user = new UserInfo();
        user.Info = await UserService.GetUserAsync(userName);
        user.Address = await AddressService.GetAddressAsync(userName);
        return user;
    }
}