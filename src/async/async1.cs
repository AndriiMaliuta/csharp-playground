using System.IO;
using System.Threading.Tasks;

public class Program
{
    public static void Main()
    {
        Program.CreateFileAsync("test.txt").Wait();
    }

    public static async Task CreateFileAsync(string filename)
    {
        using (StreamWriter writer = File.CreateText(filename))
        await writer.WriteAsync("This is a test.");
    }
}