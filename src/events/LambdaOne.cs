using System;

public class Program
{
    public static void Main()
    {
        Action hello = () => Console.WriteLine("Hello!");
        hello();
        Console.ReadKey();

        Predicate<string> validator =
            word =>
            {
            int count = word.Length;
            return count > 3;
            };
        ValidateInput(validator);
        ValidateInput(word =>
        {
            int count = word.Length;
            return count > 3;
        });
        Console.ReadKey();
    }


    public static void ValidateInput(Predicate<string> validator)
    {
        string input = "Hello";
        66bool isValid = validator(input);
        Console.WriteLine($"Is Valid: {isValid}");
    }
}